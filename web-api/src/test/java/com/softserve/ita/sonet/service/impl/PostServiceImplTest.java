package com.softserve.ita.sonet.service.impl;

import com.softserve.ita.sonet.dto.model.PostDTO;
import com.softserve.ita.sonet.exception.EntityNotFoundException;
import com.softserve.ita.sonet.model.Image;
import com.softserve.ita.sonet.model.Status;
import com.softserve.ita.sonet.model.User;
import com.softserve.ita.sonet.model.content.Post;
import com.softserve.ita.sonet.repository.PostRepo;
import com.softserve.ita.sonet.repository.UserRepo;
import org.junit.Test;
import org.junit.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;
import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@SpringBootTest
public class PostServiceImplTest {

    @Mock
    private PostRepo postRepo;

    @Mock
    private UserRepo userRepo;

    @Mock
    private ModelMapper mapper;

    @InjectMocks
    private PostServiceImpl postService;

    private Optional<Post> optionalPost;
    private Optional<User> optionalUser;
    private Post post;
    private PostDTO postDTO;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        User user = new User();
        user.setLogo(new Image());
        user.setNickname("@nick");

        List<Image> imageList = new ArrayList<>(Arrays.asList(new Image(), new Image()));

        post = new Post();
        post.setText("Text");
        post.setUpdateTime(LocalDateTime.now());
        post.setCreationTime(LocalDateTime.now());
        post.setComments(new ArrayList<>());
        post.setStatus(Status.ACTIVE);
        post.setOwner(user);
        post.setImages(imageList);

        optionalPost = Optional.of(post);
        optionalUser = Optional.empty();
        postDTO = new PostDTO();
    }

    @Test
    public void findByIdTest() {

        when(postRepo.findById(anyLong())).thenReturn(optionalPost);
        when(mapper.map(post, PostDTO.class)).thenReturn(postDTO);

        try {
            post = optionalPost.orElseThrow(() -> new RuntimeException("exception"));
        } catch (Exception e) {
            fail();
        }

        PostDTO postDTO = postService.findById(anyLong());

        assertNotNull(postDTO);
        assertEquals(post.getOwner().getNickname(), postDTO.getNickname());

    }

    @Test
    public void deleteTest() {
        when(postRepo.findById(anyLong())).thenReturn(optionalPost);
        try {
            post = optionalPost.orElseThrow(() -> new RuntimeException("exception"));

            postService.delete(anyLong());

            assertEquals(Status.DELETED,post.getStatus());

        } catch (Exception e) {
            fail();
        }
    }

    @Test(expected = EntityNotFoundException.class)
   public void findNewThrowExceptionTest(){
        when(userRepo.findById(anyLong())).thenReturn(optionalUser);
        postService.findNews(anyLong());
    }

}