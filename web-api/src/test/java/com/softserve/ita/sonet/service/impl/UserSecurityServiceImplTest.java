package com.softserve.ita.sonet.service.impl;
import com.softserve.ita.sonet.security.dto.SecuredUserDTO;
import com.softserve.ita.sonet.security.dto.RegisterUserRequest;
import com.softserve.ita.sonet.model.ActorRole;
import com.softserve.ita.sonet.model.Role;
import com.softserve.ita.sonet.model.Status;
import com.softserve.ita.sonet.model.User;
import com.softserve.ita.sonet.repository.PostRepo;
import com.softserve.ita.sonet.repository.RoleRepo;
import com.softserve.ita.sonet.repository.UserRepo;
import com.softserve.ita.sonet.security.util.SecurityUtils;
import com.softserve.ita.sonet.security.service.impl.UserSecurityServiceImpl;
import org.junit.Test;
import org.junit.*;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.mail.MessagingException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@SpringBootTest
public class UserSecurityServiceImplTest {

    @Mock
    private PostRepo postRepo;

    @Mock
    private RoleRepo roleRepo;

    @Mock
    private  SecurityUtils securityUtils;

    @Spy
    private ModelMapper mapper;

    @Mock
    private EmailServiceImpl emailService;

    @Mock
    private UserRepo userRepo;

    @Mock
    private BCryptPasswordEncoder passwordEncoder;


    @InjectMocks
    private UserSecurityServiceImpl userSecurityService;


    private SecuredUserDTO securedUserDTO;
    private Role role;
    private  User user;


    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        List<Role> roles = new ArrayList<>();
        Role role = new Role();

        role.setName("ROLE_USER");
        role.setUsers(new ArrayList<>());
        role.setId(1L);

        user = new User();
        user.setEmail("email");
        user.setPassword("12345678");
        user.setEmailVerificationToken("email");
        user.setNickname("nicknameTest");
        user.setStatus(Status.NOT_ACTIVE);

        securedUserDTO = new SecuredUserDTO();
    }
    @Test
   public void register() throws MessagingException {

        when(userRepo.existsByEmail(anyString())).thenReturn(false);
        when(roleRepo.findByName(ActorRole.ROLE_USER.name())).thenReturn(role);
        when(securityUtils.generateEmailVerificationToken(anyString())).thenReturn("emailToken");
        when(mapper.map(securedUserDTO, User.class)).thenReturn(user);
        when(userRepo.save(user)).thenReturn(user);
        doNothing().when(emailService).sendConfirm(anyString(),anyString());

        SecuredUserDTO dto = userSecurityService.register(new RegisterUserRequest());
        assertNotNull(dto);
        assertEquals(user.getEmailVerificationToken(), dto.getEmailVerificationToken());
        assertEquals(user.getNickname(), dto.getNickname());
        assertEquals(user.getStatus(), dto.getStatus());
        assertEquals(user.getRoles(),dto.getRoles());

    }

}