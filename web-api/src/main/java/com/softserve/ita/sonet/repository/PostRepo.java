package com.softserve.ita.sonet.repository;

import com.softserve.ita.sonet.model.Actor;
import com.softserve.ita.sonet.model.content.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepo extends JpaRepository<Post, Long> {

    List<Post> findAllByOwner(Actor owner);

}
