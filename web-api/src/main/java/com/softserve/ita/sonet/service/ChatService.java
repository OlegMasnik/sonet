package com.softserve.ita.sonet.service;

import com.softserve.ita.sonet.dto.model.ChatDTO;
import com.softserve.ita.sonet.dto.model.MessageDTO;
import com.softserve.ita.sonet.dto.request.CreateChatRequest;
import com.softserve.ita.sonet.exception.EntityNotFoundException;
import com.softserve.ita.sonet.model.Chat;

import java.util.List;


public interface ChatService {

  ChatDTO create(CreateChatRequest chat);

  void delete(Long chatId);

  ChatDTO getChatById(Long chatId) throws EntityNotFoundException;

  List<ChatDTO> getAllChats();

  List<MessageDTO> getAllMessages(Long chatId);

  MessageDTO getLastMessage(Long chatId);
}
