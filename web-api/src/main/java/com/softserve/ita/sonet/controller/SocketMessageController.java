package com.softserve.ita.sonet.controller;

import com.softserve.ita.sonet.dto.model.MessageDTO;
import com.softserve.ita.sonet.service.ChatService;
import com.softserve.ita.sonet.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;


@Controller
public class SocketMessageController {

    private final MessageService messageService;
    private final ChatService chatService;


    @Autowired
    public SocketMessageController(MessageService messageService, ChatService chatService) {
        this.messageService = messageService;
        this.chatService = chatService;
    }

    @MessageMapping("api/message/{id}")
    @SendTo("api/topic/{id}")
    @PreAuthorize("@userSecurityServiceImpl.isChatMember(#id)")
    public MessageDTO sendMessage(@Payload MessageDTO messageDTO, @DestinationVariable Long id){
        return messageService.save(id, messageDTO, messageDTO.getSenderId());
    }

}
