package com.softserve.ita.sonet.model.content;

import com.softserve.ita.sonet.model.User;
import com.softserve.ita.sonet.model.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import org.springframework.core.style.ToStringCreator;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "likes")
public class Like extends BaseEntity {

    @ManyToOne
    @JoinColumn(name = "post_id")
    private Post post;

    @ManyToOne
    @JoinColumn(name ="user_id")
    private User creator;

    @Column(name = "creation_time")
    @DateTimeFormat(pattern = "hh:mm:ss dd/MM/yyyy")
    protected LocalDateTime creationTime;

    @Override
    protected ToStringCreator getToStringCreator() {
        return super.getToStringCreator()
                .append("post", getPost().getId());
    }

    @PrePersist
    public void beforeSave() {
        creationTime = LocalDateTime.now();
    }
}
