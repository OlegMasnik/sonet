package com.softserve.ita.sonet.rest;

import com.softserve.ita.sonet.dto.model.ChatDTO;
import com.softserve.ita.sonet.dto.model.MessageDTO;
import com.softserve.ita.sonet.dto.request.CreateChatRequest;
import com.softserve.ita.sonet.service.impl.ChatServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/users")
public class ChatController {

    private final ChatServiceImpl chatService;


    @Autowired
    public ChatController(ChatServiceImpl chatService) {
        this.chatService = chatService;
    }

    @GetMapping
    public ResponseEntity<List<ChatDTO>> getChats() {
        return new ResponseEntity<>(new ArrayList<>(chatService.getAllChats()), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ChatDTO> createChat(@RequestBody CreateChatRequest chat){
        return ResponseEntity.ok(chatService.create(chat));
    }


    @GetMapping(path = "{userId}/chats/{id}/messages")
    @PreAuthorize("#userId == authentication.principal.id")
    public ResponseEntity<List<MessageDTO>> getMessages(@PathVariable Long id, @PathVariable Long userId) {
        return new ResponseEntity<>(chatService.getAllMessages(id), HttpStatus.OK);
    }


    @DeleteMapping(path = "{userId}/chats/{id}")
    @PreAuthorize("#userId == authentication.principal.id")
    public ResponseEntity deleteChat(@PathVariable Long id, @PathVariable Long userId) {
        chatService.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
