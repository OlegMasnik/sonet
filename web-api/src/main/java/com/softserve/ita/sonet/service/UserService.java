package com.softserve.ita.sonet.service;

import com.softserve.ita.sonet.dto.model.UserDTO;
import com.softserve.ita.sonet.dto.response.ChatResponse;
import com.softserve.ita.sonet.dto.response.UserResponse;
import com.softserve.ita.sonet.exception.EntityNotFoundException;
import com.softserve.ita.sonet.model.User;
import org.hibernate.PersistentObjectException;
import javax.validation.constraints.NotNull;
import java.util.List;

public interface UserService {

    List<ChatResponse> getAllChats(Long userId);

    List<UserResponse> getUserFollowing(Long userId);

    User getUserById(Long id) throws EntityNotFoundException;

    boolean existByEmail(String email);

    UserDTO save(@NotNull User user) throws PersistentObjectException;
}
