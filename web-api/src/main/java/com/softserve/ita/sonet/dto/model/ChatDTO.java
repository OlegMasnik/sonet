package com.softserve.ita.sonet.dto.model;

import com.softserve.ita.sonet.model.Channel;
import com.softserve.ita.sonet.model.Image;
import com.softserve.ita.sonet.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChatDTO {

    protected Long id;

    private String name;

    private LocalDateTime creationTime;

    protected LocalDateTime updateTime;

    private List<User> members;

    private List<Channel> channels;

    private Image image;

}
