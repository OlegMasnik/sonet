package com.softserve.ita.sonet.security.dto;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class LoginUserResponse {

    private Long id;
    private String email;
    private String nickname;
    private String token;
    private String firstName;
    private String lastName;
}
