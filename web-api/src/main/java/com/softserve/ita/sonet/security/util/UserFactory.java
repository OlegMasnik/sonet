package com.softserve.ita.sonet.security.util;

import com.softserve.ita.sonet.model.Image;
import com.softserve.ita.sonet.model.Status;
import com.softserve.ita.sonet.model.User;
import com.softserve.ita.sonet.repository.RoleRepo;
import com.softserve.ita.sonet.security.dto.FacebookUserInfo;
import com.softserve.ita.sonet.security.dto.GoogleUserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
public class UserFactory {

    private RoleRepo roleRepo;
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    public void setRoleRepo(RoleRepo roleRepo) {
        this.roleRepo = roleRepo;
    }

    @Autowired
    public void setPasswordEncoder(BCryptPasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public User toUserFromGoogleUser(Object object){

        if(object instanceof GoogleUserInfo){
            GoogleUserInfo googleUser = (GoogleUserInfo) object;

            User user  = new User();

            user.setRoles(Collections.singletonList(roleRepo.findByName("ROLE_USER")));
            user.setFirstName(googleUser.getName());
            user.setLastName(googleUser.getLastName());
            user.setEmail(googleUser.getEmail());
            user.setNickname(googleUser.getEmail());
            user.setAvatar(new Image(googleUser.getPhotoUrl(), Status.ACTIVE));
            user.setPassword(passwordEncoder.encode(googleUser.getId()));
            user.setGooglePassword(passwordEncoder.encode(googleUser.getId()));
            user.setStatus(Status.ACTIVE);

            return user;
        }
        if(object instanceof FacebookUserInfo){

            FacebookUserInfo facebookUser = (FacebookUserInfo) object;
            User user  = new User();

            user.setRoles(Collections.singletonList(roleRepo.findByName("ROLE_USER")));
            user.setFirstName(facebookUser.getFirstName());
            user.setLastName(facebookUser.getLastName());
            user.setEmail(facebookUser.getEmail());
            user.setNickname(facebookUser.getEmail());
            user.setPassword(passwordEncoder.encode(facebookUser.getId()));
        }

        return null;
    }
}
