package com.softserve.ita.sonet.service.impl;

import com.softserve.ita.sonet.dto.model.MessageDTO;
import com.softserve.ita.sonet.model.Channel;
import com.softserve.ita.sonet.model.Chat;
import com.softserve.ita.sonet.model.Status;
import com.softserve.ita.sonet.model.User;
import com.softserve.ita.sonet.model.content.Message;
import com.softserve.ita.sonet.repository.ChannelRepo;
import com.softserve.ita.sonet.repository.ChatRepo;
import com.softserve.ita.sonet.repository.MessageRepo;
import com.softserve.ita.sonet.repository.UserRepo;
import com.softserve.ita.sonet.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
public class MessageServiceImpl implements MessageService {

    private  MessageRepo messageRepo;
    private  ChatRepo chatRepo;
    private  UserRepo userRepo;
    private  ChannelRepo channelRepo;

    @Autowired
    public void setMessageRepo(MessageRepo messageRepo) {
        this.messageRepo = messageRepo;
    }

    @Autowired
    public void setChatRepo(ChatRepo chatRepo) {
        this.chatRepo = chatRepo;
    }

    @Autowired
    public void setUserRepo(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Autowired
    public void setChannelRepo(ChannelRepo channelRepo) {
        this.channelRepo = channelRepo;
    }

    @Override
    public MessageDTO create(Message message) {
        return null;
    }

    @Override
    public MessageDTO save(Long chatId, MessageDTO messageDTO, Long userId) {
        Message message = new Message();
        Chat chat = chatRepo.getOne(chatId);
        Channel channel = chat.getChannels().get(0);
        message.setChannel(channel);
        message.setText(messageDTO.getContent());
        message.setStatus(Status.ACTIVE);
        User user = userRepo.getOne(userId);
        message.setCreator(user);
        channel.getMessages().add(message);
        messageRepo.save(message);
        messageDTO.setSender(user.getFirstName() + " " + user.getLastName());
        messageDTO.setSendTime(LocalDateTime.now());
        return messageDTO;
    }

    @Override
    public void delete(Integer messageId) {

    }

    @Override
    public List<MessageDTO> getAllByIdSender(Integer senderId) {
        return null;
    }

    @Override
    public List<MessageDTO> getAllByIdReceiver(Integer senderId, Integer receiverId) {
        return null;
    }
}
