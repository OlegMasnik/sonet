package com.softserve.ita.sonet.dto.model;

import com.softserve.ita.sonet.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FollowsDTO {

    private User follower;

    private User following;

    protected LocalDateTime creationTime;

}
