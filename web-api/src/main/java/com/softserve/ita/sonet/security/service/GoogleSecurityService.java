package com.softserve.ita.sonet.security.service;

import com.softserve.ita.sonet.exception.GoogleAuthenticationException;
import com.softserve.ita.sonet.security.dto.GoogleUserInfo;
import com.softserve.ita.sonet.security.dto.LoginUserRequest;

public interface GoogleSecurityService {

    LoginUserRequest authenticateGoogleUser(GoogleUserInfo googleUser) throws GoogleAuthenticationException;

}
