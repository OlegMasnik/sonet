package com.softserve.ita.sonet.security.constant;

public class EmailLinkConstant {

    public static final String EMAIL_VERIFY_LINK = "/auth/email/confirmed/";
    public static final String PASSWORD_RESTORE_LINK = "/auth/password-restore/";

}

