package com.softserve.ita.sonet.service.impl;

import com.softserve.ita.sonet.dto.model.UserDTO;
import com.softserve.ita.sonet.dto.response.ChatResponse;
import com.softserve.ita.sonet.dto.response.UserResponse;
import com.softserve.ita.sonet.exception.EmailSendingException;
import com.softserve.ita.sonet.exception.EntityNotFoundException;
import com.softserve.ita.sonet.model.Chat;
import com.softserve.ita.sonet.model.User;
import com.softserve.ita.sonet.repository.UserRepo;
import com.softserve.ita.sonet.service.ChatService;
import com.softserve.ita.sonet.service.UserService;
import org.hibernate.PersistentObjectException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private ChatService chatService;
    private UserRepo userRepo;
    private ModelMapper mapper;

    @Autowired
    public void setUserRepo(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Autowired
    public void setChatService(ChatService chatService) {
        this.chatService = chatService;
    }

    @Autowired
    public void setMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public List<ChatResponse> getAllChats(Long userId) {
        User user = userRepo.getOne(userId);
        List<Chat> userChats = user.getChats();
        List<ChatResponse> result = new ArrayList<>();
        userChats.forEach(chat -> {
            if (chat.getName() == null || chat.getName().equals("")) {
                chat.getMembers().forEach(member -> {
                    if (!member.getId().equals(user.getId())) {
                        chat.setName(member.getFirstName() + " " + member.getLastName());
                    }
                });
            }
            result.add(new ChatResponse(chat.getId(), chat.getName(), chatService.getLastMessage(chat.getId())));
        });
        return result;
    }

    @Override
    public List<UserResponse> getUserFollowing(Long userId) throws EmailSendingException {
        User user = userRepo.getOne(userId);
        List<UserResponse> result = new ArrayList<>();
        user.getFollowing().forEach(follow -> result.add(new UserResponse(follow.getFollowing().getId(), follow.getFollowing().getFirstName(),
                follow.getFollowing().getLastName())));

        return result;

    }

    @Override
    public User getUserById(Long id) {
        return userRepo.findById(id)
                .orElseThrow(() ->
                        new EntityNotFoundException("User with id:{0} not found", id));
    }

    @Override
    public boolean existByEmail(String email) {
        return userRepo.existsByEmail(email);
    }

    @Override
    public UserDTO save(@NotNull User user) throws PersistentObjectException {
        User savedUser = userRepo.save(user);
        return mapper.map(savedUser, UserDTO.class);
    }
}
