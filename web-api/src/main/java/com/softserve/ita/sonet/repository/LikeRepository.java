package com.softserve.ita.sonet.repository;


import com.softserve.ita.sonet.model.content.Like;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LikeRepository extends JpaRepository<Like, Long> {

    boolean existsById(Long id);

    List<Like> findAllByPostId(Long postId);

}
