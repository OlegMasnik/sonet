package com.softserve.ita.sonet.repository;

import com.softserve.ita.sonet.model.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageRepo extends JpaRepository<Image, Long> {

    Image findImageByUrl(String Url);
}
