package com.softserve.ita.sonet.dto.model;

import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class PostDTO {

    private Long id;

    private Long ownerId;

    private String text;

    private String nickname;

    private String logoUrl;

    private List<String> images;

    private LocalDateTime updateTime;

    private LocalDateTime creationTime;

}


