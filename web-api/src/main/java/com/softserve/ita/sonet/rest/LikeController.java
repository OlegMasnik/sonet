package com.softserve.ita.sonet.rest;


import com.softserve.ita.sonet.dto.model.LikeDTO;
import com.softserve.ita.sonet.service.LikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/likes")
public class LikeController {

    private final LikeService likeService;

    @Autowired
    public LikeController(LikeService likeService) {
        this.likeService = likeService;
    }

    @PostMapping
    public ResponseEntity likeCreation(@RequestBody LikeDTO like) {
        return new ResponseEntity<>(likeService.insertLike(like), HttpStatus.CREATED);
    }

    @GetMapping("/posts/{postId}")
    public ResponseEntity getAllLikesByPostId(@PathVariable("postId") Long postId) {
        List<LikeDTO> likes = likeService.findAllByPostId(postId);
        return new ResponseEntity<>(likes, HttpStatus.OK);
    }


    @DeleteMapping("/delete/{id}")
    @PreAuthorize("@userSecurityServiceImpl.isLikeOwner(#id)")
    public ResponseEntity deleteLikeById(@PathVariable("id") Long id) {
        likeService.deleteLike(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
