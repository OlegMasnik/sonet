package com.softserve.ita.sonet.repository;

import com.softserve.ita.sonet.model.follow.Follow;
import com.softserve.ita.sonet.model.User;
import com.softserve.ita.sonet.model.follow.FollowPrimaryKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface FollowRepo extends JpaRepository<Follow, FollowPrimaryKey> {

    List<Follow> findByFollower(User user);
}
