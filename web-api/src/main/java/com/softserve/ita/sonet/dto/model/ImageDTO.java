package com.softserve.ita.sonet.dto.model;


import lombok.*;

@ToString
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ImageDTO {

    private Long id;
    private String url;

}