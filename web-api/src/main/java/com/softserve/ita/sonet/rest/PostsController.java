package com.softserve.ita.sonet.rest;

import com.softserve.ita.sonet.dto.model.PostDTO;
import com.softserve.ita.sonet.dto.request.PostAddRequest;
import com.softserve.ita.sonet.dto.request.PostUpdateRequest;
import com.softserve.ita.sonet.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/posts")
public class PostsController {

    private final PostService postService;

    @Autowired
    public PostsController(PostService postService) {
        this.postService = postService;
    }


    @GetMapping("/{postId}")
    public ResponseEntity<PostDTO> getPost(@PathVariable Long postId) {
        PostDTO postResponse = postService.findById(postId);
        return new ResponseEntity<>(postResponse, HttpStatus.OK);
    }


    @GetMapping("/users/{actorId}")
    public ResponseEntity<List<PostDTO>> getPosts(@PathVariable Long actorId) {
        List<PostDTO> postResponse = postService.findAll(actorId);
        return new ResponseEntity<>(postResponse, HttpStatus.OK);
    }


    @GetMapping("/users/{actorId}/news")
    public ResponseEntity<List<PostDTO>> getNews(@PathVariable Long actorId) {
        List<PostDTO> postResponse = postService.findNews(actorId);
        return new ResponseEntity<>(postResponse, HttpStatus.OK);
    }

    @DeleteMapping("/{postId}")
    @PreAuthorize("@userSecurityServiceImpl.isPostOwner(#postId)")
    public ResponseEntity deletePost(@PathVariable Long postId) {
        postService.delete(postId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping
    public ResponseEntity<PostDTO> createPost(@Valid @RequestBody PostAddRequest post) {
        PostDTO response = postService.create(post);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PutMapping
    @PreAuthorize("@userSecurityServiceImpl.isPostOwner(#post.id)")
    public ResponseEntity<PostDTO> updatePost(@Valid @RequestBody PostUpdateRequest post) {
        PostDTO response = postService.update(post);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


}
