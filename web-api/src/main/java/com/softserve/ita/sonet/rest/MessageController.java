package com.softserve.ita.sonet.rest;

import com.softserve.ita.sonet.dto.model.MessageDTO;
import com.softserve.ita.sonet.model.content.Message;
import com.softserve.ita.sonet.service.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("api/messages")
public class MessageController {

    private final MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @GetMapping("/users/{senderId}")
    @PreAuthorize("#senderId == authentication.principal.id")
    public ResponseEntity getMessageBySender(@PathVariable Integer senderId) {
        List<MessageDTO> messageDTOS = new ArrayList<>(messageService.getAllByIdSender(senderId));
        return new ResponseEntity<>(messageDTOS, HttpStatus.OK);

    }

    @GetMapping("/dialog/{receiverId}")
    public ResponseEntity<List<MessageDTO>> getMessageByReceiver(@PathVariable Integer senderId, @PathVariable Integer receiverId) {

        List<MessageDTO> messageDTOS = new ArrayList<>(messageService.getAllByIdReceiver(senderId, receiverId));
        return new ResponseEntity<>(messageDTOS, HttpStatus.OK);

    }

    @PostMapping
    public ResponseEntity<MessageDTO> createMessage(@Valid @RequestBody Message message) {
        MessageDTO messageDTO = messageService.create(message);
        return new ResponseEntity<>(messageDTO, HttpStatus.OK);

    }


    @DeleteMapping("/{messageId}")
    public ResponseEntity deleteMessage(@PathVariable Integer messageId) {
        messageService.delete(messageId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }
}
