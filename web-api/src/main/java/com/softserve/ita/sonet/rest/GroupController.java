package com.softserve.ita.sonet.rest;

import com.softserve.ita.sonet.dto.model.GroupDTO;
import com.softserve.ita.sonet.model.Group;
import com.softserve.ita.sonet.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/groups")
public class GroupController {

    private final GroupService groupService;

    @Autowired
    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }

    @GetMapping
    public ResponseEntity<List<GroupDTO>> getGroups() {
        List<GroupDTO> groupsResponse = groupService.getAllGroups();

        return new ResponseEntity<>(groupsResponse, HttpStatus.OK);

    }

    @GetMapping("/{groupId}")
    public ResponseEntity<GroupDTO> getGroupById(@PathVariable("groupId") Long groupId) {
        GroupDTO groupResponse = groupService.findById(groupId);
        return new ResponseEntity<>(groupResponse, HttpStatus.OK);

    }

    @GetMapping(path = "/name?q={groupName}")
    public ResponseEntity<GroupDTO> getGroupByName(@PathVariable String groupName) {
        GroupDTO groupResponse = groupService.findByName(groupName);
        return new ResponseEntity<>(groupResponse, HttpStatus.OK);

    }


    @GetMapping("/word?q={word}")
    public ResponseEntity<List<GroupDTO>> getGroupByWord(@PathVariable String word) {
        List<GroupDTO> groupsResponse = groupService.findByWord(word);
        return new ResponseEntity<>(groupsResponse, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<GroupDTO> createGroup(@Valid @RequestBody Group group) {
        GroupDTO groupResponse = groupService.create(group);
        return new ResponseEntity<>(groupResponse, HttpStatus.CREATED);

    }


    @PutMapping("/{groupId}")
    @PreAuthorize("@userSecurityServiceImpl.isGroupOwner(#groupId)")
    public ResponseEntity<GroupDTO> updateGroup(@PathVariable Long groupId, @RequestBody Group group) {
        GroupDTO groupResponse = groupService.save(groupId, group);
        return new ResponseEntity<>(groupResponse, HttpStatus.OK);

    }

    @DeleteMapping("/{groupId}")
    @PreAuthorize("@userSecurityServiceImpl.isGroupOwner(#groupId)")
    public ResponseEntity deleteGroup(@PathVariable Long groupId) {
        groupService.delete(groupId);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }


}
