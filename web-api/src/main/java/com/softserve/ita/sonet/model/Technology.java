package com.softserve.ita.sonet.model;

import com.softserve.ita.sonet.model.entity.NamedEntity;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor

@Entity
@Table(name = "technologies")
public class Technology extends NamedEntity {
}
