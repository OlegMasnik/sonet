package com.softserve.ita.sonet.model.follow;

import com.softserve.ita.sonet.model.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@NoArgsConstructor
@Getter
@Setter
public class FollowPrimaryKey implements Serializable {

    private User follower;
    private User following;
}
