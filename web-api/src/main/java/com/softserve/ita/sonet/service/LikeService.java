package com.softserve.ita.sonet.service;

import com.softserve.ita.sonet.dto.model.LikeDTO;

import java.util.List;

public interface LikeService {

    LikeDTO insertLike(LikeDTO like);

    void deleteLike(Long id);

    List <LikeDTO> findAllByPostId(Long id);

    LikeDTO findLikeById(Long likeId);
}

