package com.softserve.ita.sonet.service.impl;

import com.softserve.ita.sonet.dto.model.GroupDTO;
import com.softserve.ita.sonet.exception.EntityNotFoundException;
import com.softserve.ita.sonet.model.*;
import com.softserve.ita.sonet.repository.GroupRepo;
import com.softserve.ita.sonet.repository.RoleRepo;
import com.softserve.ita.sonet.service.GroupService;
import com.softserve.ita.sonet.service.UserService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GroupServiceImpl implements GroupService {

    private RoleRepo roleRepo;
    private GroupRepo groupRepo;
    private ModelMapper mapper;
    private UserService userService;

    @Autowired
    private void setGroupRepo(GroupRepo groupRepo) {
        this.groupRepo = groupRepo;
    }

    @Autowired
    private void setMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    @Autowired
    private void setRoleRepo(RoleRepo roleRepo) {
        this.roleRepo = roleRepo;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public GroupDTO create(Group group) {
        GroupDTO groupDTO = new GroupDTO();
        User user = new User();
        Role roleGroup = roleRepo.findByName(ActorRole.ROLE_GROUP.name());
        List<Role> groupRoles = new ArrayList<>();

        groupRoles.add(roleGroup);
        groupDTO.setId(group.getId());
        groupDTO.setName(group.getName());
        groupDTO.setNickname(group.getNickname());
        groupDTO.setDescription(group.getDescription());
        groupDTO.setCreatorId(user.getId());
        groupDTO.setRoles(groupRoles);
        groupDTO.setStatus(Status.ACTIVE);


        Group groupEntity = mapper.map(groupDTO, Group.class);

        groupEntity = groupRepo.save(groupEntity);

        return mapper.map(groupEntity, GroupDTO.class);
    }

    @Override
    public void delete(Long groupId) {
        Group group = groupRepo.findById(groupId)
                .orElseThrow(() ->
                        new EntityNotFoundException("Group with id:{0} not found", groupId));

        group.setStatus(Status.DELETED);
        groupRepo.save(group);
    }

    @Override
    public GroupDTO findById(Long groupId) {

        Group groupResponse = groupRepo.findById(groupId)
                .orElseThrow(() ->
                        new EntityNotFoundException("Group with id:{0} not found", groupId));

        GroupDTO groupDTO = mapper.map(groupResponse, GroupDTO.class);
        groupDTO.setCreatorId(groupResponse.getCreator().getId());

        return groupDTO;
    }

    @Override
    public GroupDTO findByName(String name) {
        return new GroupDTO();
    }

    @Override
    public List<GroupDTO> findByWord(String word) {
        return new ArrayList<>();
    }

    @Override
    public List<GroupDTO> getAllGroups() throws EntityNotFoundException {
        List<Group> groups = groupRepo.findAll();

        if (groups.isEmpty()) {
            throw new EntityNotFoundException("Chats not found");
        }

        return mapper.map(groups, new TypeToken<List<GroupDTO>>(){}.getType());
    }

    @Override
    public GroupDTO save(Long groupId, Group group) {
       Group groupResult  = groupRepo.save(group);

       return mapper.map(groupResult, GroupDTO.class);
    }


}
