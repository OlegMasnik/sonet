package com.softserve.ita.sonet.dto.request;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CreateChatRequest {

    Long creatorId;

    String conversationNameLabel;

    Long[] checkedFriends;

}
