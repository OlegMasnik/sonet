package com.softserve.ita.sonet.service.impl;

import com.softserve.ita.sonet.dto.model.LikeDTO;
import com.softserve.ita.sonet.dto.model.PostDTO;
import com.softserve.ita.sonet.dto.model.UserDTO;
import com.softserve.ita.sonet.exception.EntityNotFoundException;
import com.softserve.ita.sonet.model.content.Like;
import com.softserve.ita.sonet.repository.LikeRepository;
import com.softserve.ita.sonet.service.LikeService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class LikeServiceImpl implements LikeService {


    private LikeRepository likeRepository;

    private ModelMapper modelMapper;

    @Autowired
    public void setLikeRepository(LikeRepository likeRepository) {
        this.likeRepository = likeRepository;
    }

    @Autowired
    public void setModelMapper(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public LikeDTO insertLike(LikeDTO likeDTO) {

        Like like = modelMapper.map(likeDTO, Like.class);

        likeRepository.save(like);

        return likeDTO;

    }

    @Override
    public void deleteLike(Long id) {

        Like like = likeRepository.findById(id).
                orElseThrow(()-> new EntityNotFoundException("Like with id:{0} not found",id));

        likeRepository.deleteById(id);
    }

    @Override
    public List<LikeDTO> findAllByPostId(Long id) {

        List<Like> likeEntities = likeRepository.findAllByPostId(id);

        List<LikeDTO> likeDTOS = new ArrayList<LikeDTO>();

        for (Like like : likeEntities) {
            likeDTOS.add(modelMapper.map(like, LikeDTO.class));
        }

        return likeDTOS;
    }

    @Override
    public LikeDTO findLikeById(Long likeId) {

      Like like = likeRepository.findById(likeId).
               orElseThrow(()-> new EntityNotFoundException("Like with id:{0} not found",likeId));

       LikeDTO likeDTO = new LikeDTO();

       UserDTO userDTO = modelMapper.map(like.getCreator(), UserDTO.class);
       PostDTO postDTO = modelMapper.map(like.getPost(), PostDTO.class);

       likeDTO.setId(likeId);
       likeDTO.setCreationTime(like.getCreationTime());
       likeDTO.setCreator(userDTO);
       likeDTO.setPost(postDTO);

       return likeDTO;
    }
}
