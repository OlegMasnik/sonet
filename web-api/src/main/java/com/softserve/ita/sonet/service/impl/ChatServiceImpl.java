package com.softserve.ita.sonet.service.impl;

import com.softserve.ita.sonet.dto.model.ChatDTO;
import com.softserve.ita.sonet.dto.model.MessageDTO;
import com.softserve.ita.sonet.dto.request.CreateChatRequest;
import com.softserve.ita.sonet.exception.EntityNotFoundException;
import com.softserve.ita.sonet.model.Channel;
import com.softserve.ita.sonet.model.Chat;
import com.softserve.ita.sonet.model.User;
import com.softserve.ita.sonet.model.content.Message;
import com.softserve.ita.sonet.repository.*;
import com.softserve.ita.sonet.service.ChatService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;

@RequiredArgsConstructor
@Service
public class ChatServiceImpl implements ChatService {

    private ChatRepo chatRepo;
    private ChannelRepo channelRepo;
    private MessageRepo messageRepo;
    private UserRepo userRepo;
    private ImageRepo imageRepo;
    private ModelMapper mapper;

    @Autowired
    public void setChatRepo(ChatRepo chatRepo) {
        this.chatRepo = chatRepo;
    }

    @Autowired
    public void setChannelRepo(ChannelRepo channelRepo) {
        this.channelRepo = channelRepo;
    }

    @Autowired
    public void setMessageRepo(MessageRepo messageRepo) {
        this.messageRepo = messageRepo;
    }

    @Autowired
    public void setUserRepo(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Autowired
    public void setImageRepo(ImageRepo imageRepo) {
        this.imageRepo = imageRepo;
    }

    @Autowired
    public void setMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public ChatDTO create(CreateChatRequest chatRequest) {

        Chat chat = new Chat();
        Long creatorId = chatRequest.getCreatorId();
        chat.setName(chatRequest.getConversationNameLabel());

        User creator = userRepo.getOne(creatorId);

        if(creator == null){
            throw new NoSuchElementException("Cannot find user with id:" + creatorId);
        }

        chat.setCreator(creator);
        List<User> members = new ArrayList<>();

        for (Long checkedFriend : chatRequest.getCheckedFriends()) {
            User user = userRepo.getOne(checkedFriend);
            if(user == null){
                throw new NoSuchElementException("Cannot find user with id:" + checkedFriend);
            }
            members.add(user);
            user.getChats().add(chat);
        }

        members.add(creator);
        creator.getChats().add(chat);
        chat.setMembers(members);
        Channel channel = new Channel();
        channel.setName("stub");
        channel.setType("stub");
        channel.setChat(chat);
        chat.setChannels(new ArrayList<>(Collections.singleton(channel)));
        chatRepo.save(chat);
        channelRepo.save(channel);

        return mapper.map(chat, ChatDTO.class);
    }

    @Override
    public void delete(Long chatId) {

    }

    @Override
    public ChatDTO getChatById(Long chatId) throws EntityNotFoundException {
        Chat chatResponse = chatRepo.findById(chatId)
                .orElseThrow(()->new EntityNotFoundException("Chat with id: " + chatId + " not found"));

        return mapper.map(chatResponse, ChatDTO.class);
    }

    @Override
    public List<ChatDTO> getAllChats() throws EntityNotFoundException {
        List<Chat> chats = chatRepo.findAll();

        if(chats.isEmpty()){
            throw new EntityNotFoundException("Chats not found");
        }

        Type listType = new TypeToken<List<ChatDTO>>() {}.getType();
        return mapper.map(chats, listType);
    }

    @Override
    public List<MessageDTO> getAllMessages(Long chatId) {
        List<MessageDTO> result = new ArrayList<>();
        Chat chat = chatRepo.getOne(chatId);
        Channel channel = chat.getChannels().get(0);
        messageRepo.findMessagesByChannel(channel).forEach(message-> result.add(new MessageDTO(
                message.getId(),
                message.getText(),
                message.getCreator().getFirstName() + " " + message.getCreator().getLastName(),
                message.getCreator().getId(), message.getCreationTime())));
        return result;
    }

    @Override
    public MessageDTO getLastMessage(Long chatId){
        Chat chat = chatRepo.getOne(chatId);
        Message message = messageRepo.getTop1ByChannelOrderByCreationTimeDesc(chat.getChannels().get(0));
        if(message == null){
            return null;
        }
        return new MessageDTO(message.getId(), message.getText(), message.getCreator().getFirstName() + " "
                + message.getCreator().getLastName(), message.getCreator().getId(), message.getCreationTime());
    }
}
