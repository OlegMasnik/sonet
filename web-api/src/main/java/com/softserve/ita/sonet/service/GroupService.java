package com.softserve.ita.sonet.service;

import com.softserve.ita.sonet.dto.model.GroupDTO;
import com.softserve.ita.sonet.exception.EntityNotFoundException;
import com.softserve.ita.sonet.model.Group;

import java.util.List;

public interface GroupService {

    GroupDTO create(Group group);

    void delete(Long groupId) throws EntityNotFoundException;

    GroupDTO findById(Long groupId) throws EntityNotFoundException;

    GroupDTO findByName(String name) throws EntityNotFoundException;

    List<GroupDTO> findByWord(String word) throws EntityNotFoundException;

    List<GroupDTO> getAllGroups() throws EntityNotFoundException;

    GroupDTO save(Long groupId, Group group);

}
