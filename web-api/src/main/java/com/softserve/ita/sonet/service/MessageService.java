package com.softserve.ita.sonet.service;

import com.softserve.ita.sonet.dto.model.MessageDTO;
import com.softserve.ita.sonet.model.User;
import com.softserve.ita.sonet.model.content.Message;

import java.util.List;

public interface MessageService {

    MessageDTO create(Message message);

    MessageDTO save(Long id, MessageDTO messageDTO, Long userId);

    void delete(Integer messageId);

    List<MessageDTO> getAllByIdSender(Integer senderId);

    List<MessageDTO> getAllByIdReceiver(Integer senderId, Integer receiverId);

}
