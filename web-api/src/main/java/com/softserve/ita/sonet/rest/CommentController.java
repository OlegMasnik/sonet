package com.softserve.ita.sonet.rest;

import com.softserve.ita.sonet.dto.model.CommentDTO;
import com.softserve.ita.sonet.dto.request.CreateCommentRequest;
import com.softserve.ita.sonet.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/comments")
public class CommentController {

    private final CommentService commentService;

    @Autowired
    public CommentController(CommentService commentService) {
        this.commentService = commentService;
    }

    @PostMapping
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity createComment(@RequestBody CreateCommentRequest request) {
        CommentDTO commentDTO = commentService.saveComment(request);

        return new ResponseEntity<>(commentDTO, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity getAllComments() {
        List<CommentDTO> comments = commentService.findAll();
        return new ResponseEntity<>(comments, HttpStatus.OK);
    }

    @GetMapping("/posts/{postId}")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity getAllCommentsByPostId(@PathVariable("postId") Long postId) {
        List<CommentDTO> comments = commentService.findAllCommentsByPostId(postId);
        return new ResponseEntity<>(comments, HttpStatus.OK);
    }

    @GetMapping("/posts/postId/users/userId")
    public ResponseEntity getAllCommentsByPostIdAndUserId(@RequestParam("postId") Long postId, @RequestParam("userId") Long userId) {
        List<CommentDTO> comments = commentService.findAllCommentsByPostIdAndCreatorId(postId, userId);
        return new ResponseEntity<>(comments, HttpStatus.OK);
    }

    @GetMapping("/{commentId}")
    public ResponseEntity getCommentById(@PathVariable("commentId") Long commentId) {
        CommentDTO commentDTO = commentService.findCommentById(commentId);

        return new ResponseEntity<>(commentDTO, HttpStatus.OK);
    }

    @PatchMapping("/{commentId}")
    @PreAuthorize("@userSecurityServiceImpl.isCommentOwner(#commentId)")
    public ResponseEntity updateComment(@PathVariable("commentId") Long commentId, @RequestBody CommentDTO commentToUpdate) {
        CommentDTO commentDTO = commentService.updateComment(commentId, commentToUpdate);

        return new ResponseEntity<>(commentDTO, HttpStatus.OK);
    }

    @DeleteMapping("/{commentId}")
    @PreAuthorize("@userSecurityServiceImpl.isCommentOwner(#commentId)")
    public ResponseEntity deleteCommentById(@PathVariable("commentId") Long commentId) {
        commentService.deleteCommentById(commentId);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
