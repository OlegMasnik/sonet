package com.softserve.ita.sonet.dto.model;

import lombok.Data;

@Data
public class UserDTO {

    Long id;

    String email;

    String firstName;

    String lastName;

    String city;

    String planet;

    String country;

}
