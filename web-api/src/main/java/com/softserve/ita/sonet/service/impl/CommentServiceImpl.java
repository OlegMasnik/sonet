package com.softserve.ita.sonet.service.impl;

import com.softserve.ita.sonet.dto.model.CommentDTO;
import com.softserve.ita.sonet.dto.request.CreateCommentRequest;
import com.softserve.ita.sonet.exception.EntityNotFoundException;
import com.softserve.ita.sonet.model.User;
import com.softserve.ita.sonet.model.content.Comment;
import com.softserve.ita.sonet.model.content.Post;
import com.softserve.ita.sonet.repository.CommentRepo;
import com.softserve.ita.sonet.service.CommentService;
import com.softserve.ita.sonet.service.UserService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.function.Supplier;


@Service
public class CommentServiceImpl implements CommentService {

    private CommentRepo commentRepo;
    private ModelMapper modelMapper;
    private UserService userService;

    @Autowired
    public void setCommentRepo(CommentRepo commentRepo) {
        this.commentRepo = commentRepo;
    }

    @Autowired
    void setModelMapper(ModelMapper modelMapper){
        this.modelMapper = modelMapper;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Transactional
    @Override
    public CommentDTO saveComment(CreateCommentRequest request) {

        User user = userService.getUserById(request.getCreatorId());

        Comment comment = new Comment();
        Post post = new Post();

        post.setId(request.getPostId());

        comment.setUpdateTime(LocalDateTime.now());
        comment.setCreationTime(LocalDateTime.now());
        comment.setCreator(user);
        comment.setText(request.getText());
        comment.setPost(post);

        comment = commentRepo.save(comment);

        return modelMapper.map(comment, CommentDTO.class);
    }


    @Override
    public List<CommentDTO> findAllCommentsByPostId(Long postId) {
        List<Comment> commentEntities = commentRepo.findAllByPostId(postId);

        if(commentEntities.isEmpty()){
            return Collections.emptyList();
        }

        return modelMapper.map(commentEntities,
                new TypeToken<List<CommentDTO>>(){}.getType());
    }

    @Override
    public List<CommentDTO> findAllCommentsByPostIdAndCreatorId(Long postId, Long userId) {
        List<Comment> commentEntities = commentRepo.findAllByPostIdAndCreatorId(postId, userId);

        return modelMapper.map(commentEntities,
        new TypeToken<List<CommentDTO>>(){}.getType());
    }

    @Override
    public void deleteCommentById(Long id) throws EntityNotFoundException {
        try {
            commentRepo.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new EntityNotFoundException("Comment with id:{0} not found", id);
        }
    }

    @Override
    public List<CommentDTO> findAll() throws EntityNotFoundException{

        List<Comment> commentEntities = commentRepo.findAll();

        if(commentEntities.isEmpty()){
            throw new EntityNotFoundException("Cannot find any comments");
        }

        return modelMapper.map(commentEntities,
                new TypeToken<List<CommentDTO>>(){}.getType());

    }


    @Override
    public CommentDTO findCommentById(Long id) throws EntityNotFoundException {
        Comment comment = commentRepo.findById(id)
                .orElseThrow(getCommentNotFoundException(id));

        return modelMapper.map(comment, CommentDTO.class);
    }

    @Override
    public CommentDTO updateComment(Long id, CommentDTO commentToUpdate) throws EntityNotFoundException{

        Comment commentFromBD = commentRepo
                .findById(id)
                .orElseThrow(getCommentNotFoundException(id));

        commentFromBD.setText(commentToUpdate.getText());

        commentFromBD.setId(id);

        commentFromBD = commentRepo.save(commentFromBD);

        return modelMapper.map(commentFromBD, CommentDTO.class);

    }

    private Supplier<EntityNotFoundException> getCommentNotFoundException(Long id) {
        return () -> new EntityNotFoundException("Comment with id:{0} not found", id);
    }


}
