package com.softserve.ita.sonet.security.service.impl;

import com.softserve.ita.sonet.security.dto.SecuredUserDTO;
import com.softserve.ita.sonet.security.jwt.JwtUserFactory;
import com.softserve.ita.sonet.security.service.UserSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class SecurityUserDetailsService implements UserDetailsService {


    private UserSecurityService userSecurityService;

    @Autowired
    public void setUserSecurityService(UserSecurityService userSecurityService) {
        this.userSecurityService = userSecurityService;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        SecuredUserDTO userDTO = userSecurityService.findByEmail(email);
        return JwtUserFactory.userToJwtUser(userDTO);
    }

}


