package com.softserve.ita.sonet.repository;

import com.softserve.ita.sonet.model.Channel;
import com.softserve.ita.sonet.model.Chat;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface ChannelRepo extends JpaRepository<Channel, Long> {

    List<Channel> findAllByChat(Chat chat);
}
