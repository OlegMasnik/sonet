package com.softserve.ita.sonet.repository;

import com.softserve.ita.sonet.model.Channel;
import com.softserve.ita.sonet.model.content.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface MessageRepo extends JpaRepository<Message, Long> {

    Message getTop1By();

    List<Message> findMessagesByChannel(Channel channel);


    Message getTop1ByChannelOrderByCreationTimeDesc(Channel channel);


}
