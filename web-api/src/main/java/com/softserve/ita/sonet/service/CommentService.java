package com.softserve.ita.sonet.service;

import com.softserve.ita.sonet.dto.model.CommentDTO;
import com.softserve.ita.sonet.dto.request.CreateCommentRequest;

import java.util.List;

public interface CommentService {

    List<CommentDTO> findAllCommentsByPostId(Long postId);

    List<CommentDTO> findAllCommentsByPostIdAndCreatorId(Long postId, Long userId);

    List<CommentDTO> findAll();

    CommentDTO findCommentById(Long id);

    CommentDTO updateComment(Long id, CommentDTO commentToUpdate);

    void deleteCommentById(Long id);

    CommentDTO saveComment(CreateCommentRequest request);

}
