package com.softserve.ita.sonet.dto.model;


import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Data
@Getter
@Setter
@NoArgsConstructor
public class LikeDTO {

    private Long id;

    private LocalDateTime creationTime;

    private PostDTO post;

    private UserDTO creator;


}
