package com.softserve.ita.sonet.service.impl;

import com.softserve.ita.sonet.dto.model.PostDTO;
import com.softserve.ita.sonet.dto.request.PostAddRequest;
import com.softserve.ita.sonet.dto.request.PostUpdateRequest;
import com.softserve.ita.sonet.exception.EntityNotFoundException;
import com.softserve.ita.sonet.model.*;
import com.softserve.ita.sonet.model.content.Post;
import com.softserve.ita.sonet.model.entity.CreatedEntity;
import com.softserve.ita.sonet.model.follow.Follow;
import com.softserve.ita.sonet.repository.*;
import com.softserve.ita.sonet.service.PostService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;


@Service
public class PostServiceImpl implements PostService {

    private PostRepo postRepo;
    private ActorRepo actorRepo;
    private ModelMapper mapper;
    private FollowRepo followRepo;
    private ImageRepo imageRepo;
    private UserRepo userRepo;

    @Autowired
    public void setPostRepo(PostRepo postRepo) {
        this.postRepo = postRepo;
    }

    @Autowired
    public void setActorRepo(ActorRepo actorRepo) {
        this.actorRepo = actorRepo;
    }

    @Autowired
    public void setMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    @Autowired
    public void setFollowRepo(FollowRepo followRepo) {
        this.followRepo = followRepo;
    }

    @Autowired
    public void setUserRepo(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Autowired
    public void setImageRepo(ImageRepo imageRepo) {
        this.imageRepo = imageRepo;
    }

    private PostDTO toDoPostDTO(Post post) {
        PostDTO postDTO = mapper.map(post, PostDTO.class);

        postDTO.setOwnerId(post.getOwner().getId());
        postDTO.setNickname(post.getOwner().getNickname());

        postDTO.setLogoUrl(post.getOwner()
                .getLogo()
                .getUrl());

        postDTO.setImages(post.getImages()
                .stream()
                .map(Image::getUrl)
                .collect(Collectors.toList()));

        return postDTO;
    }


    @Override
    @Transactional
    public PostDTO findById(Long id) throws DataAccessException, EntityNotFoundException {
        Post post = postRepo.findById(id).orElseThrow(() ->
                new EntityNotFoundException("Post with id:{0} not found", id));

        if (post.getStatus().equals(Status.DELETED)) {
            throw new EntityNotFoundException("Post with id:{0} not found", id);
        }

        return toDoPostDTO(post);
    }

    @Override
    @Transactional
    public PostDTO create(PostAddRequest postAddRequest) throws DataAccessException {

        Actor actor = actorRepo
                .findById(postAddRequest.getIdActor())
                .orElseThrow(() ->
                        new EntityNotFoundException("User with id:{0} not found",
                                postAddRequest.getIdActor()));

        Post post = new Post();

        post.setCreationTime(LocalDateTime.now());
        post.setUpdateTime(LocalDateTime.now());
        post.setCreator((User) actor);
        post.setOwner(actor);
        post.setText(postAddRequest.getText());

        post.setImages(postAddRequest.getImages()
                .stream()
                .map(imageURL -> imageRepo.findImageByUrl(imageURL))
                .collect(Collectors.toList()));

        post.setStatus(Status.ACTIVE);

        post = postRepo.save(post);

        return toDoPostDTO(post);

    }


    @Override
    public void delete(Long id) {
        Post post = postRepo
                .findById(id)
                .orElseThrow(() ->
                        new EntityNotFoundException("Post with id:{0} not exists", id));

        post.setStatus(Status.DELETED);
        postRepo.save(post);
    }

    @Override
    public PostDTO update(PostUpdateRequest postUpdateRequest) {

        Post postEntity = postRepo
                .findById(postUpdateRequest.getId())
                .orElseThrow(() ->
                        new EntityNotFoundException("Post with id:{0} not exists", postUpdateRequest.getId()));

        if (postEntity.getStatus().equals(Status.DELETED)) {
            throw new EntityNotFoundException("Post with id:{0} not exists", postEntity.getId());
        }

        postEntity.setImages(postUpdateRequest.getImages()
                .stream()
                .map(imageURL -> imageRepo.findImageByUrl(imageURL))
                .collect(Collectors.toList()));

        postEntity.setText(postUpdateRequest.getText());
        postEntity.setUpdateTime(LocalDateTime.now());

        postEntity = postRepo.save(postEntity);

        return toDoPostDTO(postEntity);
    }

    @Override
    @Transactional
    public List<PostDTO> findNews(Long idUser) {

        User user = userRepo.findById(idUser).orElseThrow(() ->
                new EntityNotFoundException("User with id:{0} not found", idUser));

        List<Follow> following = followRepo.findByFollower(user);

        List<User> follows = new ArrayList<>();

        for (Follow follow : following) {
            follows.add(follow.getFollowing());
        }

        List<Post> news = new ArrayList<>();


        follows.forEach(tmpUser -> tmpUser.getOwnPosts()
                .stream()
                .filter(tmpPost -> tmpPost.getStatus().equals(Status.ACTIVE))
                .collect(Collectors.toCollection(() -> news))
        );

        news.sort(Comparator.comparing(CreatedEntity::getCreationTime).reversed());

        return news.stream()
                .map(this::toDoPostDTO)
                .collect(Collectors.toList());

    }


    @Override
    @Transactional
    public List<PostDTO> findAll(Long idActor) {

        Actor actor = actorRepo.findById(idActor).orElseThrow(() ->
                new EntityNotFoundException("User with id:{0} not found", idActor));

        List<Post> posts = postRepo.findAllByOwner(actor);

        if (posts.isEmpty()) {
            throw new EntityNotFoundException("Actor with id:{0} don`t have any posts", idActor);
        }

        return posts.stream()
                .filter(tmpPost -> tmpPost.getStatus().equals(Status.ACTIVE))
                .map(this::toDoPostDTO)
                .collect(Collectors.toList());

    }


}
