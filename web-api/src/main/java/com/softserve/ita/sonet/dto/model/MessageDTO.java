package com.softserve.ita.sonet.dto.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MessageDTO {

    private Long id;

    private String content;

    private String sender;

    private Long senderId;

    private LocalDateTime sendTime;

}
