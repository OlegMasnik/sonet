package com.softserve.ita.sonet.security.dto;

import lombok.Data;

@Data
public class FacebookUserInfo {

    private String authToken;

    private String email;

    private String firstName;

    private String lastName;

    private String id;

    private String idToken;

    private String nickName;

    private String photoUrl;

    private String provider;
}
