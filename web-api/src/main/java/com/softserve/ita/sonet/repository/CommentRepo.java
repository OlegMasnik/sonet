package com.softserve.ita.sonet.repository;


import com.softserve.ita.sonet.model.content.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepo
        extends JpaRepository<Comment, Long> {

    boolean existsById(Long id);

    List<Comment> findAllByPostId(Long postId);

    List<Comment> findAllByPostIdAndCreatorId(Long postId, Long userId);
}
