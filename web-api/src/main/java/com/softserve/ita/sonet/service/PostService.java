package com.softserve.ita.sonet.service;

import com.softserve.ita.sonet.dto.model.PostDTO;
import com.softserve.ita.sonet.dto.request.PostAddRequest;
import com.softserve.ita.sonet.dto.request.PostUpdateRequest;
import org.springframework.dao.DataAccessException;

import java.util.List;

public interface PostService {

    PostDTO findById(Long id) throws DataAccessException;

    PostDTO create(PostAddRequest postAddRequest) throws DataAccessException;

    void delete(Long id);

    PostDTO update(PostUpdateRequest postUpdateRequest);

    List<PostDTO> findNews(Long idUser);

    List<PostDTO> findAll(Long idActor);

}
