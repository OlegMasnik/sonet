package com.softserve.ita.sonet.security.service;

import com.softserve.ita.sonet.exception.EmailAlreadyExistException;
import com.softserve.ita.sonet.exception.EmailSendingException;
import com.softserve.ita.sonet.exception.EntityNotFoundException;
import com.softserve.ita.sonet.security.dto.RegisterUserRequest;
import com.softserve.ita.sonet.security.dto.SecuredUserDTO;
import org.springframework.dao.DataAccessException;


public interface UserSecurityService {

    SecuredUserDTO register(RegisterUserRequest requestUser) throws DataAccessException, EmailAlreadyExistException;

    SecuredUserDTO findByEmail(String username) throws EntityNotFoundException;

    SecuredUserDTO findById(Long id) throws EntityNotFoundException;

    boolean verifyEmailToken(String token);

    boolean resetPasswordRequest(String email) throws EmailSendingException;

    boolean resetPassword(String token, String password) throws EmailSendingException;

    boolean isActive(String email) throws EntityNotFoundException;

    boolean isGroupOwner(Long groupId);

    boolean isCommentOwner(Long commentId);

    boolean isLikeOwner(Long likeId);

    boolean isPostOwner(Long postId);

    boolean isChatMember(Long chatId);

}
