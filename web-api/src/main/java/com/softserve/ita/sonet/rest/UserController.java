package com.softserve.ita.sonet.rest;


import com.softserve.ita.sonet.dto.response.ChatResponse;
import com.softserve.ita.sonet.dto.response.UserResponse;
import com.softserve.ita.sonet.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController()
@RequestMapping("api/users")
public class UserController {

    private  UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(path = "/{id}/conversations")
    @PreAuthorize("#id == authentication.principal.id")
    public ResponseEntity<List<ChatResponse>> getConversations(@PathVariable Long id) {
        return new ResponseEntity<>(userService.getAllChats(id), HttpStatus.OK);
    }

    @GetMapping(path = "/{id}/following")
    public ResponseEntity<List<UserResponse>> getFollowing(@PathVariable Long id){
        return new ResponseEntity<>(userService.getUserFollowing(id), HttpStatus.OK);
    }


}
