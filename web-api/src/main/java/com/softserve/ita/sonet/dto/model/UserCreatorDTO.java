package com.softserve.ita.sonet.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



@Getter
@Setter
@NoArgsConstructor
public class UserCreatorDTO {

    private Long id;

    private String nickname;

    private String firstName;

    private String lastName;
}
