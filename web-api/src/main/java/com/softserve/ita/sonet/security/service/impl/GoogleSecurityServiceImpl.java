package com.softserve.ita.sonet.security.service.impl;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.softserve.ita.sonet.exception.GoogleAuthenticationException;
import com.softserve.ita.sonet.model.Image;
import com.softserve.ita.sonet.model.Status;
import com.softserve.ita.sonet.model.User;
import com.softserve.ita.sonet.security.dto.GoogleUserInfo;
import com.softserve.ita.sonet.security.dto.LoginUserRequest;
import com.softserve.ita.sonet.security.dto.SecuredUserDTO;
import com.softserve.ita.sonet.security.service.GoogleSecurityService;
import com.softserve.ita.sonet.security.service.UserSecurityService;
import com.softserve.ita.sonet.security.util.UserFactory;
import com.softserve.ita.sonet.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Collections;

@PropertySource("classpath:security_resource/security.properties")
@Service
public class GoogleSecurityServiceImpl implements GoogleSecurityService {

    private final GoogleIdTokenVerifier verifier;
    private final UserSecurityService userSecurityService;
    private final UserService userService;
    private final UserFactory userFactory;
    private final BCryptPasswordEncoder passwordEncoder;
    private final ModelMapper mapper;


    @Autowired
    public GoogleSecurityServiceImpl(
            @Value("${google.client-id}")String googleClientId,
            UserSecurityService userSecurityService,
            UserService userService,
            UserFactory userFactory,
            BCryptPasswordEncoder passwordEncoder,
            ModelMapper mapper
    ) {
        this.userSecurityService = userSecurityService;
        this.userService = userService;
        this.userFactory = userFactory;
        this.passwordEncoder = passwordEncoder;
        this.mapper = mapper;
        this.verifier = new GoogleIdTokenVerifier
                .Builder(new NetHttpTransport(), new JacksonFactory())
                .setAudience(Collections.singletonList(googleClientId))
                .build();
    }

    @Override
    @Transactional
    public LoginUserRequest authenticateGoogleUser(GoogleUserInfo googleUser) throws GoogleAuthenticationException {

        if (googleUser == null) {
            throw new GoogleAuthenticationException("Google user data cannot be null");
        }

        if (!isValidToken(googleUser.getIdToken())) {
            throw new GoogleAuthenticationException("Google client id token has been expired");
        }
        
        LoginUserRequest request = new LoginUserRequest();

        if (!userService.existByEmail(googleUser.getEmail())) {

            User userNew = userFactory.toUserFromGoogleUser(googleUser);
            userService.save(userNew);

            request.setEmail(googleUser.getEmail());
            request.setPassword(googleUser.getId());

        } else {

            SecuredUserDTO userUpdate = userSecurityService.findByEmail(googleUser.getEmail());

            if (userUpdate.getGooglePassword() == null) {

                userUpdate.setGooglePassword(passwordEncoder.encode(googleUser.getId()));
                userUpdate.setStatus(Status.ACTIVE);

                if (userUpdate.getAvatar() == null) {
                    userUpdate.setAvatar(new Image(googleUser.getPhotoUrl(), Status.ACTIVE));
                }

                userService.save(mapper.map(userUpdate, User.class));
            }
            request.setEmail(googleUser.getEmail());
            request.setPassword(googleUser.getId());
        }

        return request;
    }

    private boolean isValidToken(String clientIdToken) throws GoogleAuthenticationException {

        if (clientIdToken != null) {
            GoogleIdToken idToken;
            try {
                idToken = verifier.verify(clientIdToken);
                return idToken != null;
            } catch (GeneralSecurityException | IOException e) {
                throw new GoogleAuthenticationException("Error while validation client id token");
            }
        } else {
            throw new GoogleAuthenticationException("Google token cannot be null");
        }
    }
}
