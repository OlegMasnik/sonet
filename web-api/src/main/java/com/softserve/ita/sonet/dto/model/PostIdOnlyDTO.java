package com.softserve.ita.sonet.dto.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PostIdOnlyDTO {
    private Long id;
}
