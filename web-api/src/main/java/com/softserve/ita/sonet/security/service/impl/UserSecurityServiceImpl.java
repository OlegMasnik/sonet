package com.softserve.ita.sonet.security.service.impl;

import com.softserve.ita.sonet.exception.EmailAlreadyExistException;
import com.softserve.ita.sonet.exception.EmailSendingException;
import com.softserve.ita.sonet.exception.EntityNotFoundException;
import com.softserve.ita.sonet.model.*;
import com.softserve.ita.sonet.repository.PasswordResetTokenRepo;
import com.softserve.ita.sonet.repository.RoleRepo;
import com.softserve.ita.sonet.repository.UserRepo;
import com.softserve.ita.sonet.security.dto.JwtUser;
import com.softserve.ita.sonet.security.dto.RegisterUserRequest;
import com.softserve.ita.sonet.security.dto.SecuredUserDTO;
import com.softserve.ita.sonet.security.service.UserSecurityService;
import com.softserve.ita.sonet.security.util.SecurityUtils;
import com.softserve.ita.sonet.service.*;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static com.softserve.ita.sonet.security.constant.EmailLinkConstant.EMAIL_VERIFY_LINK;
import static com.softserve.ita.sonet.security.constant.EmailLinkConstant.PASSWORD_RESTORE_LINK;

@Service
public class UserSecurityServiceImpl implements UserSecurityService {

    private static final Logger logger = LoggerFactory.getLogger(UserSecurityServiceImpl.class);

    @Value("${web.url}")
    private String WEB_END_POINT;

    private RoleRepo roleRepo;
    private UserRepo userRepo;
    private BCryptPasswordEncoder passwordEncoder;
    private ModelMapper mapper;
    private SecurityUtils securityUtils;
    private EmailService emailService;
    private PasswordResetTokenRepo resetTokenRepository;
    private GroupService groupService;
    private CommentService commentService;
    private LikeService likeService;
    private PostService postService;
    private ChatService chatService;

    @Autowired
    public UserSecurityServiceImpl(
            RoleRepo roleRepo,
            UserRepo userRepo,
            BCryptPasswordEncoder passwordEncoder,
            ModelMapper mapper,
            @Lazy SecurityUtils securityUtils,
            @Lazy EmailService emailService,
            @Lazy PasswordResetTokenRepo resetTokenRepository,
            @Lazy GroupService groupService,
            @Lazy CommentService commentService,
            @Lazy LikeService likeService,
            @Lazy PostService postService,
            @Lazy ChatService chatService
    ) {
        this.roleRepo = roleRepo;
        this.userRepo = userRepo;
        this.passwordEncoder = passwordEncoder;
        this.mapper = mapper;
        this.securityUtils = securityUtils;
        this.emailService = emailService;
        this.resetTokenRepository = resetTokenRepository;
        this.groupService = groupService;
        this.commentService = commentService;
        this.likeService = likeService;
        this.postService = postService;
        this.chatService = chatService;
    }

    @Override
    @Transactional
    public SecuredUserDTO register(RegisterUserRequest requestUser) throws EmailAlreadyExistException, EmailSendingException {

        if (userRepo.existsByEmail(requestUser.getEmail())) {
            throw new EmailAlreadyExistException("This email already exists");
        }

        SecuredUserDTO securedUserDTO = new SecuredUserDTO();
        String email = requestUser.getEmail();

        Role roleUser = roleRepo.findByName(ActorRole.ROLE_USER.name());

        List<Role> userRoles = new ArrayList<>();
        userRoles.add(roleUser);
        securedUserDTO.setEmail(email);
        securedUserDTO.setNickname(requestUser.getNickname());
        securedUserDTO.setPassword(passwordEncoder.encode(requestUser.getPassword()));
        securedUserDTO.setRoles(userRoles);
        securedUserDTO.setStatus(Status.NOT_ACTIVE);

        String emailVerifyToken = securityUtils
                .generateEmailVerificationToken(email);
        securedUserDTO.setEmailVerificationToken(emailVerifyToken);

        User userEntity = mapper.map(securedUserDTO, User.class);

        userEntity = userRepo.save(userEntity);

        try {
            logger.warn(getEmailVerificationLink(emailVerifyToken));
            emailService.sendConfirm(email, getEmailVerificationLink(emailVerifyToken));
        } catch (MessagingException e) {
            throw new EmailSendingException("Error while sending email", e);
        }

        return mapper.map(userEntity, SecuredUserDTO.class);
    }

    @Override
    public SecuredUserDTO findByEmail(String email) throws DataAccessException, EntityNotFoundException {

        User user = userRepo
                .findOptionalByEmail(email)
                .orElseThrow(() ->
                        new EntityNotFoundException("User with email:{0} not found", email));

        return mapper.map(user, SecuredUserDTO.class);

    }

    @Override
    public SecuredUserDTO findById(Long id) throws DataAccessException, EntityNotFoundException {

        User user = userRepo.findById(id)
                .orElseThrow(() ->
                        new EntityNotFoundException("User with id:{0} not found", id));

        return mapper.map(user, SecuredUserDTO.class);

    }

    @Override
    public boolean verifyEmailToken(String token) {

        if (securityUtils.hasTokenExpired(token)) {
            return false;
        }

        User user = userRepo.findUserByEmailVerificationToken(token);

        if (user == null) {
            return false;
        }
        user.setStatus(Status.ACTIVE);

        user = userRepo.save(user);
        return user.getEmailVerificationToken() != null;

    }


    @Override
    public boolean resetPasswordRequest(String email) {

        User user = userRepo.findUserByEmail(email);

        if (user == null) {
            return false;
        }

        String token = securityUtils.generateEmailVerificationToken(email);

        PasswordResetToken passwordResetTokenEntity = new PasswordResetToken();
        passwordResetTokenEntity.setToken(token);
        passwordResetTokenEntity.setUser(user);
        resetTokenRepository.save(passwordResetTokenEntity);

        try {

            emailService.sendRestorePassword(email, getPasswordResetToken(token));

        } catch (MessagingException e) {
            throw new EmailSendingException("Error while sending email", e);
        }

        return true;
    }

    @Override
    public boolean resetPassword(String token, String password) throws EmailSendingException {

        if (token == null) {
            return false;
        }

        PasswordResetToken passwordResetTokenEntity = resetTokenRepository.findByToken(token);

        if (passwordResetTokenEntity == null) {
            return false;
        }

        String encodedPassword = passwordEncoder.encode(password);

        User userEntity = passwordResetTokenEntity.getUser();
        userEntity.setPassword(encodedPassword);

        User savedUserEntity = userRepo.save(userEntity);

        boolean isSuccess = false;
        if (savedUserEntity.getPassword().equalsIgnoreCase(encodedPassword)) {
            isSuccess = true;
        }

        resetTokenRepository.delete(passwordResetTokenEntity);

        return isSuccess;
    }

    @Override
    public boolean isActive(String email) throws EntityNotFoundException {
        User user = userRepo.findOptionalByEmail(email)
                .orElseThrow(() ->
                        new EntityNotFoundException("User with email:{0} not found", email));

        return user.getStatus().equals(Status.ACTIVE);
    }

    @Override
    public boolean isGroupOwner(Long groupId) {
        return groupService
                .findById(groupId)
                .getCreatorId().equals(getCurrentAuthUser().getId());
    }

    @Override
    public boolean isCommentOwner(Long commentId) {
        return commentService
                .findCommentById(commentId)
                .getCreator()
                .getId().equals(getCurrentAuthUser().getId());
    }

    @Override
    public boolean isLikeOwner(Long likeId) {
        return likeService
                .findLikeById(likeId)
                .getCreator()
                .getId().equals(getCurrentAuthUser().getId());
    }

    @Override
    public boolean isPostOwner(Long postId) {
        return postService
                .findById(postId)
                .getOwnerId()
                .equals(getCurrentAuthUser().getId());
    }

    @Override
    public boolean isChatMember(Long chatId) {

        Long authUserId = getCurrentAuthUser().getId();

        return chatService.getChatById(chatId)
                .getMembers()
                .stream()
                .map(member -> member.getId()
                        .equals(authUserId)).count() > 0;
    }

    private JwtUser getCurrentAuthUser(){
        return (JwtUser) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
    }

    private String getEmailVerificationLink(String token) {
        return WEB_END_POINT + EMAIL_VERIFY_LINK + token;
    }

    private String getPasswordResetToken(String token) {
        return WEB_END_POINT + PASSWORD_RESTORE_LINK + token;
    }


}
