package com.softserve.ita.sonet.config;

import com.softserve.ita.sonet.security.jwt.JwtConfigurer;
import com.softserve.ita.sonet.security.jwt.JwtTokenProvider;
import com.softserve.ita.sonet.security.service.impl.CustomAuthenticationProvider;
import com.softserve.ita.sonet.security.service.impl.SecurityUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;
import java.util.Collections;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Value("${web.origins}")
    private String[] origins;

    private static final String PERMIT_ALL_ENDPOINT = "/api/auth/**";
    private static final String ADMIN_ENDPOINT = "/api/admins/**";
    private static final String MODERATOR_ENDPOINT = "/api/moderators/**";
    private static final String USER_ENDPOINT = "/api/users/**";
    private static final String COMMENTS_ENDPOINT = "/api/comments/**";
    private static final String CHATS_ENDPOINT = "/api/chats/**";
    private static final String POSTS_ENDPOINT = "/api/posts/**";
    private static final String GROUPS_ENDPOINT = "/api/groups/**";
    private static final String SPRING_ADMIN_ACTUATOR_ENDPOINT = "/actuator/**";

    private final JwtTokenProvider jwtTokenProvider;
    private final SecurityUserDetailsService securityUserDetailsService;
    private final PasswordEncoder passwordEncoder;
    private final CustomAuthenticationProvider customAuthenticationProvider;

    @Autowired
    public SecurityConfig(JwtTokenProvider jwtTokenProvider,
                          SecurityUserDetailsService securityUserDetailsService,
                          PasswordEncoder passwordEncoder,
                          CustomAuthenticationProvider customAuthenticationProvider
    ) {
        this.jwtTokenProvider = jwtTokenProvider;
        this.securityUserDetailsService = securityUserDetailsService;
        this.passwordEncoder = passwordEncoder;
        this.customAuthenticationProvider = customAuthenticationProvider;
    }


    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    public void configure(AuthenticationManagerBuilder authenticationManagerBuilder) throws Exception {
        authenticationManagerBuilder
                .authenticationProvider(customAuthenticationProvider)
                .userDetailsService(securityUserDetailsService)
                .passwordEncoder(passwordEncoder);
    }


    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.cors().and().csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.authorizeRequests()
                .antMatchers(PERMIT_ALL_ENDPOINT, SPRING_ADMIN_ACTUATOR_ENDPOINT).permitAll()
                .antMatchers(PERMIT_ALL_ENDPOINT).permitAll()
                .antMatchers(MODERATOR_ENDPOINT).hasRole("MODERATOR")
                .antMatchers(ADMIN_ENDPOINT).hasRole("ADMIN")
                .antMatchers(USER_ENDPOINT).hasAnyRole("USER","MODERATOR","ADMIN")
                .antMatchers(COMMENTS_ENDPOINT).hasAnyRole("USER", "MODERATOR", "ADMIN")
                .antMatchers(CHATS_ENDPOINT).hasAnyRole("USER", "MODERATOR", "ADMIN")
                .antMatchers(POSTS_ENDPOINT).hasAnyRole("USER", "MODERATOR", "ADMIN")
                .antMatchers(GROUPS_ENDPOINT).hasAnyRole("USER", "MODERATOR", "ADMIN")
                .anyRequest().authenticated()
                .and()
                .apply(new JwtConfigurer(jwtTokenProvider));
    }


    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        final CorsConfiguration configuration = new CorsConfiguration();

        configuration.setAllowedOrigins(Arrays.asList(origins));
        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"));
        configuration.setAllowCredentials(true);
        configuration.setAllowedHeaders(Collections.singletonList("*"));

        final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);

        return source;
    }
}