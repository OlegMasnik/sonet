package com.softserve.ita.sonet.security.service;

import com.softserve.ita.sonet.security.dto.FacebookUserInfo;
import com.softserve.ita.sonet.security.dto.LoginUserRequest;

public interface FacebookSecurityService {

    LoginUserRequest authenticateFacebookUser(FacebookUserInfo facebookUser) throws IllegalArgumentException;
}
