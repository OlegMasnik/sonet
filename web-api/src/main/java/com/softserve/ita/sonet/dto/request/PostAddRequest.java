package com.softserve.ita.sonet.dto.request;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.constraints.Size;
import java.util.List;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PostAddRequest {

    @JsonProperty("actor_id")
    private Long idActor;

    @Size(max = 500)
    private String text;

    @Size(max = 4)
    private List<String> images;

}
