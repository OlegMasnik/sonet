package com.softserve.ita.sonet.dto.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.softserve.ita.sonet.model.Role;
import com.softserve.ita.sonet.model.Status;
import lombok.Data;

import java.util.List;

@JsonIgnoreProperties
@Data
public class GroupDTO {

    protected Long id;

    private String name;

    private String nickname;

    private String description;

    private Long creatorId;

    private Status status;

    private List<Role> roles;


}
