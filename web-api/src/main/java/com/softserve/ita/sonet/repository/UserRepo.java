package com.softserve.ita.sonet.repository;

import com.softserve.ita.sonet.model.User;
import org.springframework.dao.DataAccessException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<User, Long> {

    Optional<User> findOptionalByEmail(String email) throws DataAccessException;

    User findUserByEmailVerificationToken(String token) throws DataAccessException;

    @Query(value = "DELETE FROM user_chats WHERE chat_id = ? AND user_id = ?", nativeQuery = true)
    User deleteChatFromUserIdAndChatsId(Long chatId, Long userId) throws DataAccessException;

    boolean existsByEmail(String email) throws DataAccessException;

    User findUserByEmail(String email) throws DataAccessException;


}


